import React from 'react';
import { FaTwitter, FaLinkedin, FaInstagram } from 'react-icons/fa';
import { SiGitlab } from 'react-icons/si';

const Footer = () => {
  return (
    <footer className="bg-[#283055] text-white py-12 px-6 border-t-2">
      <div className="container mx-auto flex flex-col md:flex-row justify-between items-center space-y-4 md:space-y-0">
        {/* Información de derechos de autor */}
        <div className="flex-1 text-center md:text-left text-xl font-semibold">
          <p>&copy; {new Date().getFullYear()} Daniel Gomez. Todos los derechos reservados.</p>
        </div>

        {/* Enlaces de navegación */}
        <div className="flex-1 text-center md:text-right space-x-6">
          <a
            href="#aboutme"
            className="text-lg transition-all duration-300 ease-in-out hover:text-[#4c6ef3] hover:underline"
            aria-label="About me"
          >
            About
          </a>
          <a
            href="#portfolio"
            className="text-lg transition-all duration-300 ease-in-out hover:text-[#4c6ef3] hover:underline"
            aria-label="Portfolio"
          >
            Portfolio
          </a>
          <a
            href="#contact"
            className="text-lg transition-all duration-300 ease-in-out hover:text-[#4c6ef3] hover:underline"
            aria-label="Contact"
          >
            Contact
          </a>
        </div>

        {/* Redes sociales */}
        <div className="flex-1 flex justify-center space-x-6">
          <a
            href="https://x.com/danielG_mz"
            className="text-[#1DA1F2] transition-all duration-300 ease-in-out transform hover:scale-110 hover:rotate-12 hover:shadow-lg"
            aria-label="Twitter"
            target="_blank"
            rel="noopener noreferrer"
          >
            <FaTwitter className="w-8 h-8" />
          </a>
          <a
            href="https://gitlab.com/danielGomz"
            className="text-[#FCA121] transition-all duration-300 ease-in-out transform hover:scale-110 hover:rotate-12 hover:shadow-lg"
            aria-label="GitLab"
            target="_blank"
            rel="noopener noreferrer"
          >
            <SiGitlab className="w-8 h-8" />
          </a>
          <a
            href="https://www.linkedin.com/in/danielGomz"
            className="text-[#0A66C2] transition-all duration-300 ease-in-out transform hover:scale-110 hover:rotate-12 hover:shadow-lg"
            aria-label="LinkedIn"
            target="_blank"
            rel="noopener noreferrer"
          >
            <FaLinkedin className="w-8 h-8" />
          </a>
          <a
            href="https://instagram.com/danielg.mz"
            className="text-[#E4405F] transition-all duration-300 ease-in-out transform hover:scale-110 hover:rotate-12 hover:shadow-lg"
            aria-label="Instagram"
            target="_blank"
            rel="noopener noreferrer"
          >
            <FaInstagram className="w-8 h-8" />
          </a>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
