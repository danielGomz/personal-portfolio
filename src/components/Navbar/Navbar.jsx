import React, { useState, useEffect, useRef } from 'react';
import { motion } from 'framer-motion';

const Navbar = () => {
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const menuRef = useRef(null);
  const buttonRef = useRef(null);

  const toggleMenu = () => {
    setIsMenuOpen(!isMenuOpen);
  };

  const handleClickOutside = (event) => {
    if (
      menuRef.current && !menuRef.current.contains(event.target) &&
      buttonRef.current && !buttonRef.current.contains(event.target)
    ) {
      setIsMenuOpen(false);
    }
  };

  const handleResize = () => {
    if (window.innerWidth >= 768) {
      setIsMenuOpen(false);
    }
  };

  useEffect(() => {
    if (isMenuOpen) {
      document.addEventListener('mousedown', handleClickOutside);
    } else {
      document.removeEventListener('mousedown', handleClickOutside);
    }

    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [isMenuOpen]);

  useEffect(() => {
    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  return (
    <motion.nav
      className="bg-[#283055] p-4 fixed w-full h-16 z-20"
      initial={{ opacity: 0, y: -20 }}
      animate={{ opacity: 1, y: 0 }}
      transition={{ duration: 1, ease: 'easeOut' }}
      role="navigation"
      aria-label="Main navigation"
    >
      <div className="container mx-auto flex justify-between items-center">
        {/* Logo */}
        <a href="#home" className="text-white text-xl font-bold" title="Go to homepage">
          DanielGomz
          <motion.span
            className="inline-block w-[0.375rem] h-[0.375rem] ml-1"
            animate={{ backgroundColor: ["#4d6bfd", "#6d28d9", "#ec4899", "#14b8a6", "#4d6bfd"] }}
            transition={{ repeat: Infinity, duration: 4, ease: "linear" }}
          />
        </a>

        <div className="hidden md:flex space-x-6 items-center">
          <motion.a
            href="#aboutme"
            className="text-gray-300 hover:text-white px-3"
            initial={{ opacity: 0, x: -20 }}
            animate={{ opacity: 1, x: 0 }}
            transition={{ duration: 0.5, delay: 0.3 }}
            aria-label="About me section"
          >
            About me
          </motion.a>
          <motion.a
            href="#services"
            className="text-gray-300 hover:text-white px-3"
            initial={{ opacity: 0, x: -20 }}
            animate={{ opacity: 1, x: 0 }}
            transition={{ duration: 0.5, delay: 0.4 }}
            aria-label="Services section"
          >
            Services
          </motion.a>
          <motion.a
            href="#portfolio"
            className="text-gray-300 hover:text-white px-3"
            initial={{ opacity: 0, x: -20 }}
            animate={{ opacity: 1, x: 0 }}
            transition={{ duration: 0.5, delay: 0.5 }}
            aria-label="Portfolio section"
          >
            Portfolio
          </motion.a>
          <motion.a
            href="#skills"
            className="text-gray-300 hover:text-white px-3"
            initial={{ opacity: 0, x: -20 }}
            animate={{ opacity: 1, x: 0 }}
            transition={{ duration: 0.5, delay: 0.6 }}
            aria-label="Skills section"
          >
            Skills
          </motion.a>
          <motion.a
            href="#contact"
            className="text-[#3b57e2] hover:bg-[#e0e0e0] bg-[#fdfbfe] px-3 py-2 font-bold shadow-2xl"
            initial={{ opacity: 0, x: -20 }}
            animate={{ opacity: 1, x: 0 }}
            whileHover={{
              scale: 1.05,
              boxShadow: '0 10px 15px -3px rgba(0, 0, 0, 0.1), 0 4px 6px -4px rgba(0, 0, 0, 0.1)'
            }}
            transition={{ duration: 0.3 }}
            aria-label="Contact me section"
          >
            Contact Me
          </motion.a>
        </div>

        <div className="md:hidden">
          <button
            ref={buttonRef}
            onClick={toggleMenu}
            className="text-gray-300 focus:outline-none"
            aria-label={isMenuOpen ? "Close menu" : "Open menu"}
          >
            <svg
              className="w-6 h-6"
              fill="none"
              stroke="currentColor"
              viewBox="0 0 24 24"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M4 6h16M4 12h16m-7 6h7"
              />
            </svg>
          </button>
        </div>
      </div>

      <div
        ref={menuRef}
        role="navigation"
        aria-hidden={!isMenuOpen}
        className={`transition-transform duration-700 ${isMenuOpen ? 'translate-x-0' : 'translate-x-full'} md:hidden mt-2 absolute left-0 w-full h-screen backdrop-blur-2xl`}
      >
        <a href="#aboutme" onClick={toggleMenu} className="block text-gray-300 hover:text-white p-2" aria-label="About me section">
          About me
        </a>
        <a href="#services" onClick={toggleMenu} className="block text-gray-300 hover:text-white p-2" aria-label="Services section">
          Services
        </a>
        <a href="#portfolio" onClick={toggleMenu} className="block text-gray-300 hover:text-white p-2" aria-label="Portfolio section">
          Portfolio
        </a>
        <a href="#skills" onClick={toggleMenu} className="block text-gray-300 hover:text-white p-2" aria-label="Skills section">
          Skills
        </a>
        <a href="#contact" onClick={toggleMenu} className="block text-gray-300 hover:text-white p-2" aria-label="Contact section">
          Contact
        </a>
      </div>
    </motion.nav>
  );
}

export default Navbar;
