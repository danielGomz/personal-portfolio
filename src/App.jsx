import { Footer, Navbar } from "./components"
import { Home } from "./pages"

function App() {

  return (
    <div className='bg-[#283055]'>
      <Navbar></Navbar>
      <Home></Home>
      <Footer></Footer>
    </div>
  )
}

export default App
