import React, { useState } from 'react';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import emailjs from '@emailjs/browser';

const validationSchema = Yup.object({
  user_name: Yup.string()
    .min(2, 'Name must be at least 2 characters')
    .required('Name is required'),
  user_email: Yup.string()
    .email('Invalid email address')
    .required('Email is required'),
  message: Yup.string()
    .min(10, 'Message must be at least 10 characters')
    .required('Message is required'),
});

const Contact = () => {
  const [status, setStatus] = useState('idle');
  const [toastMessage, setToastMessage] = useState('');

  const sendEmail = (values) => {
    setStatus('sending');
    emailjs
      .send('contact_service', 'contact_template', values, '1tpXPQY3-Wg6-zWWP')
      .then(
        () => {
          setStatus('success');
          setToastMessage('Message Sent Successfully! 🎉');
          setTimeout(() => setStatus('idle'), 3000);
          setTimeout(() => setToastMessage(''), 5000); // Clear toast message after 5 seconds
        },
        () => {
          setStatus('error');
          setToastMessage('Error! Try Again. ❌');
          setTimeout(() => setStatus('idle'), 3000);
          setTimeout(() => setToastMessage(''), 5000); // Clear toast message after 5 seconds
        }
      );
  };

  return (
    <section id="contact" className="text-white py-16 px-8 bg-[#283055] min-h-screen flex items-center justify-center">
      <div className="container mx-auto">
        <h2 className="text-4xl font-bold mb-8 text-center">Contact Me</h2>

        {/* Toast notification */}
        {toastMessage && (
          <div
            className={`absolute top-4 right-4 p-4 rounded-lg text-white ${status === 'success' ? 'bg-green-500' : 'bg-red-500'} transition-all duration-300`}
            aria-live="polite"
          >
            {toastMessage}
          </div>
        )}

        <Formik
          initialValues={{
            user_name: '',
            user_email: '',
            message: '',
          }}
          validationSchema={validationSchema}
          onSubmit={(values, { setSubmitting, resetForm }) => {
            sendEmail(values);
            setSubmitting(false);
            resetForm();
          }}
        >
          {({ isSubmitting }) => (
            <Form className="max-w-lg mx-auto">
              {/* Name Field */}
              <div className="mb-6">
                <label htmlFor="user_name" className="block text-lg mb-2">
                  Name
                </label>
                <Field
                  type="text"
                  id="user_name"
                  name="user_name"
                  className="w-full p-3 bg-white text-[#2a3259] border border-[#4c6ef3] focus:outline-none focus:ring-2 focus:ring-[#4c6ef3]"
                />
                <ErrorMessage name="user_name" component="div" className="text-red-500 text-sm" />
              </div>

              <div className="mb-6">
                <label htmlFor="user_email" className="block text-lg mb-2">
                  Email
                </label>
                <Field
                  type="email"
                  id="user_email"
                  name="user_email"
                  className="w-full p-3 bg-white text-[#2a3259] border border-[#4c6ef3] focus:outline-none focus:ring-2 focus:ring-[#4c6ef3]"
                />
                <ErrorMessage name="user_email" component="div" className="text-red-500 text-sm" />
              </div>

              <div className="mb-6">
                <label htmlFor="message" className="block text-lg mb-2">
                  Message
                </label>
                <Field
                  as="textarea"
                  id="message"
                  name="message"
                  rows="6"
                  className="w-full p-3 bg-white text-[#2a3259] border border-[#4c6ef3] focus:outline-none focus:ring-2 focus:ring-[#4c6ef3]"
                />
                <ErrorMessage name="message" component="div" className="text-red-500 text-sm" />
              </div>

              <button
                type="submit"
                disabled={isSubmitting || status === 'sending'}
                className={`py-2 px-4 font-semibold text-white shadow-2xl hover:scale-105 transition-all duration-300 
                  ${status === 'sending'
                    ? 'bg-gray-400 cursor-wait'
                    : status === 'success'
                      ? 'bg-green-500 hover:bg-green-600'
                      : status === 'error'
                        ? 'bg-red-500 hover:bg-red-600'
                        : 'bg-[#4d6bfd] hover:bg-[#3a5cbb]'
                  }`}
              >
                {status === 'sending'
                  ? 'Sending...'
                  : status === 'success'
                    ? 'Sent Successfully! 🎉'
                    : status === 'error'
                      ? 'Error! Try Again. ❌'
                      : 'Send Message'}
              </button>
            </Form>
          )}
        </Formik>
      </div>
    </section>
  );
};

export default Contact;
