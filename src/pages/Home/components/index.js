export { default as Hero } from './Hero/Hero';
export { default as About } from './About/About';
export { default as Contact } from './Contact/Contact';
export { default as Portfolio } from './Portfolio/Portfolio';
export { default as Services } from './Services/Services';
export { default as Skills } from './Skills/Skills';