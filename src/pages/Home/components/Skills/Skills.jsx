import React from 'react';
import { FaPython, FaDatabase, FaChartLine, FaGithub, FaDocker } from 'react-icons/fa';
import { SiMicrosoftazure, SiPytorch, SiJupyter, SiPowerbi } from 'react-icons/si';

const Skills = () => {
  return (
    <section id='skills' className="text-white py-16 px-8">
      <div className="container mx-auto">
        {/* Título */}
        <h2 className="text-4xl font-bold mb-8 text-center">Skills & Technologies</h2>

        {/* Habilidades Principales */}
        <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-8">
          {/* Habilidad 1 */}
          <div className="bg-[#3b4d6f] p-6 rounded-lg shadow-lg text-center" aria-label="Azure Machine Learning">
            <SiMicrosoftazure className="text-[#0089D6] text-5xl mb-4 mx-auto" aria-hidden="true" />
            <h3 className="text-2xl font-semibold mb-2">Azure Machine Learning</h3>
            <p className="text-lg">
              Skilled in deploying and managing machine learning models using Azure ML.
            </p>
          </div>

          {/* Habilidad 2 */}
          <div className="bg-[#3b4d6f] p-6 rounded-lg shadow-lg text-center" aria-label="Python">
            <FaPython className="text-[#FCA121] text-5xl mb-4 mx-auto" aria-hidden="true" />
            <h3 className="text-2xl font-semibold mb-2">Python</h3>
            <p className="text-lg">
              Expert in Python for data analysis, machine learning, and automation.
            </p>
          </div>

          {/* Habilidad 3 */}
          <div className="bg-[#3b4d6f] p-6 rounded-lg shadow-lg text-center" aria-label="Power BI">
            <SiPowerbi className="text-[#f2c811] text-5xl mb-4 mx-auto" aria-hidden="true" />
            <h3 className="text-2xl font-semibold mb-2">Power BI</h3>
            <p className="text-lg">
              Skilled in creating interactive dashboards and data-driven insights with Power BI.
            </p>
          </div>

          {/* Habilidad 4 */}
          <div className="bg-[#3b4d6f] p-6 rounded-lg shadow-lg text-center" aria-label="Docker">
            <FaDocker className="text-[#0db7ed] text-5xl mb-4 mx-auto" aria-hidden="true" />
            <h3 className="text-2xl font-semibold mb-2">Docker</h3>
            <p className="text-lg">
              Knowledgeable in containerization with Docker for scalable applications.
            </p>
          </div>

          {/* Habilidad 5 */}
          <div className="bg-[#3b4d6f] p-6 rounded-lg shadow-lg text-center" aria-label="Git & Version Control">
            <FaGithub className="text-[#b3b3b3] text-5xl mb-4 mx-auto" aria-hidden="true" />
            <h3 className="text-2xl font-semibold mb-2">Git & Version Control</h3>
            <p className="text-lg">
              Experienced with Git for version control and collaborative development.
            </p>
          </div>

          {/* Habilidad 6 */}
          <div className="bg-[#3b4d6f] p-6 rounded-lg shadow-lg text-center" aria-label="PyTorch">
            <SiPytorch className="text-[#ec4c3d] text-5xl mb-4 mx-auto" aria-hidden="true" />
            <h3 className="text-2xl font-semibold mb-2">PyTorch</h3>
            <p className="text-lg">
              Skilled in PyTorch for dynamic neural network modeling and research.
            </p>
          </div>

          {/* Habilidad 7 */}
          <div className="bg-[#3b4d6f] p-6 rounded-lg shadow-lg text-center" aria-label="SQL & Databases">
            <FaDatabase className="text-[#6ab0f3] text-5xl mb-4 mx-auto" aria-hidden="true" />
            <h3 className="text-2xl font-semibold mb-2">SQL & Databases</h3>
            <p className="text-lg">
              Experienced with SQL databases for data management and querying.
            </p>
          </div>

          {/* Habilidad 8 */}
          <div className="bg-[#3b4d6f] p-6 rounded-lg shadow-lg text-center" aria-label="Data Visualization">
            <FaChartLine className="text-[#4caf50] text-5xl mb-4 mx-auto" aria-hidden="true" />
            <h3 className="text-2xl font-semibold mb-2">Data Visualization</h3>
            <p className="text-lg">
              Proficient in creating impactful visualizations with tools like Matplotlib and Seaborn.
            </p>
          </div>

          {/* Habilidad 9 */}
          <div className="bg-[#3b4d6f] p-6 rounded-lg shadow-lg text-center" aria-label="Jupyter Notebooks">
            <SiJupyter className="text-[#f7b93e] text-5xl mb-4 mx-auto" aria-hidden="true" />
            <h3 className="text-2xl font-semibold mb-2">Jupyter Notebooks</h3>
            <p className="text-lg">
              Skilled in using Jupyter Notebooks for data analysis and model prototyping.
            </p>
          </div>
        </div>
      </div>
    </section>
  );
}

export default Skills;
