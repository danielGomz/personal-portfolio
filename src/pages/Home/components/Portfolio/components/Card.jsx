import React from 'react';
import { FaGitlab, FaLaptopCode } from 'react-icons/fa';

const Card = ({ project }) => {
  return (
    <div className="flex flex-row bg-[#2c385e] rounded-lg shadow-lg hover:shadow-xl transition-all duration-300 transform hover:scale-105 overflow-hidden">
      <div className="w-1/3 sm:h-auto bg-[#3a4d8a] flex items-center justify-center order-1">
        {project.image ? (
          <img
            src={project.image}
            alt={project.title}
            className="object-cover w-full h-full"
          />
        ) : (
          <div className="text-5xl text-white">{project.icon}</div>
        )}
      </div>

      {/* Contenido */}
      <div className="flex flex-col justify-between p-6 w-2/3">
        <div>
          <h3 className="text-2xl font-bold text-white mb-2">{project.title}</h3>
          <p className="text-sm text-[#b0b3d6] mb-4">{project.description}</p>
          <p className="text-xs text-[#9abff6]">Tools: {project.stack.join(', ')}</p>
        </div>

        {/* Botones */}
        <div className="flex flex-wrap gap-2 mt-4">
          {project.gitlab && (
            <a
              href={project.gitlab}
              target="_blank"
              rel="noopener noreferrer"
              className="flex items-center justify-center gap-2 bg-[#343c60] text-white py-2 px-3 rounded-md shadow hover:bg-[#2a2e4d] transition w-full"
            >
              <FaGitlab className="text-[#FCA121]" />
              GitLab
            </a>
          )}
          {project.live && (
            <a
              href={project.live}
              target="_blank"
              rel="noopener noreferrer"
              className="flex items-center justify-center gap-2 bg-[#4d6bfd] text-white py-2 px-3 rounded-md shadow hover:bg-[#3a5cbb] transition w-full"
            >
              <FaLaptopCode />
              View Project
            </a>
          )}
          {project.powerbi && (
            <a
              href={project.powerbi}
              target="_blank"
              rel="noopener noreferrer"
              className="flex items-center justify-center gap-2 bg-[#E9A11D] text-white py-2 px-3 rounded-md shadow hover:bg-[#d08f1a] transition w-full"
            >
              <FaLaptopCode />
              Power BI
            </a>
          )}
        </div>

      </div>
    </div>
  );
};

export default Card;
