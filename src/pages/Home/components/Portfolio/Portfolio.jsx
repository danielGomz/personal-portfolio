import React, { useState, useEffect } from 'react';
import { projectsData } from './data/ProjectsData.jsx';
import Card from './components/Card.jsx';
import { motion } from 'framer-motion';
import { FaCog } from 'react-icons/fa';

const Portfolio = () => {
  const [visibleCount, setVisibleCount] = useState(2);

  useEffect(() => {
    const updateVisibleCount = () => {
      const width = window.innerWidth;
      if (width >= 1024) {
        setVisibleCount(3);
      } else if (width >= 768) {
        setVisibleCount(2);
      } else {
        setVisibleCount(1);
      }
    };

    updateVisibleCount();
    window.addEventListener('resize', updateVisibleCount);

    return () => window.removeEventListener('resize', updateVisibleCount);
  }, []);

  const handleShowMore = () => {
    setVisibleCount(projectsData.length);
  };

  return (
    <section
      id="portfolio"
      className="text-white py-16 px-4 sm:px-8 min-h-screen flex items-center justify-center"
    >
      <div className="container mx-auto">
        <header className="text-center mb-8">
          <h2 className="text-3xl sm:text-4xl font-bold mb-4">My Projects Portfolio</h2>
          <p className="text-base sm:text-lg text-[#b0b3d6]">
            Explore the range of projects I have worked on, including web development, data science, and machine learning applications.
          </p>
        </header>

        {projectsData.length === 0 ? (
          <div className="flex flex-col items-center">
            {/* Engranaje Animado */}
            <motion.div
              animate={{ rotate: 360 }}
              transition={{ repeat: Infinity, duration: 2, ease: 'linear' }}
              className="text-6xl text-blue-500"
            >
              <FaCog />
            </motion.div>

            {/* Mensaje de texto animado */}
            <motion.h2
              initial={{ opacity: 0, y: 20 }}
              animate={{ opacity: 1, y: 0 }}
              transition={{ duration: 0.8 }}
              className="text-2xl font-bold mt-6"
            >
              Working on something great!
            </motion.h2>

            <motion.p
              initial={{ opacity: 0, y: 20 }}
              animate={{ opacity: 1, y: 0 }}
              transition={{ duration: 0.8, delay: 0.3 }}
              className="text-lg text-gray-400 mt-2"
            >
              New projects are on the way. Stay tuned!
            </motion.p>
          </div>
        ) : (
          <>
            <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-6 sm:gap-8">
              {projectsData.slice(0, visibleCount).map((project) => (
                <Card key={project.id} project={project} />
              ))}
            </div>

            {visibleCount < projectsData.length && (
              <div className="text-center mt-8">
                <button
                  onClick={handleShowMore}
                  className="bg-[#4d6bfd] text-white py-3 px-6 rounded-lg hover:bg-[#3a5cbb] hover:scale-105 transition duration-300"
                  aria-label="See more projects"
                >
                  See more
                </button>
              </div>
            )}
          </>
        )}
      </div>
    </section>
  );
};

export default Portfolio;
