import React from 'react';
import { motion } from 'framer-motion';
import { FaLongArrowAltRight } from "react-icons/fa";
import miImagen from "@/assets/profile.png";

const Hero = () => {
  return (
    <header id="home" className="min-h-screen flex flex-col-reverse pt-16">
      <div className="flex-1 flex flex-col justify-start items-center text-center text-white p-8">
        <motion.h1
          className="text-3xl md:text-5xl font-bold mb-4"
          initial={{ opacity: 0, y: -50 }}
          animate={{ opacity: 1, y: 0 }}
          transition={{ duration: 1, ease: 'easeOut' }}
        >
          <motion.span
            className="inline-block mr-3 -z-10"
            animate={{
              rotate: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0],
              scale: [1, 1.05, 1]
            }}
            transition={{
              repeat: Infinity,
              duration: 1.5,
              ease: 'easeInOut'
            }}
          >
            👋
          </motion.span>
          
          Hello, I'm Daniel Gomez
        </motion.h1>

        <motion.p
          className="text-base md:text-2xl md:w-3/4 mb-6 text-[#8f94bc]"
          initial={{ opacity: 0, y: -20 }}
          animate={{ opacity: 1, y: 0 }}
          transition={{ duration: 1, ease: 'easeOut', delay: 0.3 }}
        >
          Data Scientist & Machine Learning Engineer. I help businesses and organizations transform data into actionable insights through advanced statistical modeling, and data visualizations.
        </motion.p>

        <motion.div
          className="flex space-x-4"
          initial={{ opacity: 0, scale: 0.9 }}
          animate={{ opacity: 1, scale: 1 }}
          transition={{ duration: 0.5, delay: 0.6 }}
        >
          <a
            href="#portfolio"
            className="bg-[#4d6bfd] py-2 px-4 font-semibold text-white shadow-2xl hover:bg-[#3a5cbb] hover:scale-105 transition duration-300"
            aria-label="Explore my portfolio"
          >
            Explore My Work
          </a>

          <a
            href="#contact"
            className="bg-[#343c60] py-2 px-4 font-semibold text-white shadow-2xl hover:bg-[#2a2e4d] hover:scale-105 transition duration-300 flex items-center"
            aria-label="Contact me"
          >
            Let's Talk
            <FaLongArrowAltRight className="ml-2" />
          </a>
        </motion.div>
      </div>

      <div className="flex-[0.2] md:flex-[0.7] flex flex-col justify-center items-center p-4 md:p-8">
        <motion.div
          className="w-32 h-32 md:w-64 md:h-64 rounded-full relative overflow-hidden transition-all"
          initial={{ opacity: 0 }}
          animate={{ opacity: 1 }}
          transition={{ duration: 1.5 }}
        >
          <div
            className="absolute inset-0 rounded-full border-4 md:border-8 border-transparent animate-gradient"
            style={{ backgroundSize: '200% 200%' }}
          >
            <img
              src={miImagen}
              alt="Daniel Gomez profile picture"
              className="w-full h-full object-cover rounded-full"
            />
          </div>
        </motion.div>
      </div>
    </header>
  );
};

export default Hero;
