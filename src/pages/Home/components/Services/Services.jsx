import React from 'react';
import { FaChartLine, FaCogs, FaDatabase, FaRegLightbulb, FaCog, FaChartPie } from 'react-icons/fa';

const Services = () => {
  const services = [
    {
      icon: <FaChartLine className="text-[#9abff6] text-4xl" />,
      title: "Data Analysis",
      description: "I perform exploratory and advanced data analysis to identify patterns and trends using effective visualizations.",
    },
    {
      icon: <FaCogs className="text-[#9abff6] text-4xl" />,
      title: "Machine Learning",
      description: "I create custom machine learning models to solve specific business challenges.",
    },
    {
      icon: <FaDatabase className="text-[#9abff6] text-4xl" />,
      title: "Big Data",
      description: "I utilize advanced technologies for efficient management and processing of large volumes of data.",
    },
    {
      icon: <FaRegLightbulb className="text-[#9abff6] text-4xl" />,
      title: "Consulting",
      description: "I develop data-driven strategies to help your business grow and adapt.",
    },
    {
      icon: <FaCog className="text-[#9abff6] text-4xl" />,
      title: "Automation",
      description: "I implement automation solutions to enhance your business efficiency.",
    },
    {
      icon: <FaChartPie className="text-[#9abff6] text-4xl" />,
      title: "Visualization",
      description: "I create interactive visualizations to effectively communicate complex data.",
    },
  ];
  

  return (
    <section id='services' className="text-white py-16 px-8 min-h-screen flex items-center justify-center">
      <div className="container mx-auto">
        <h2 className="text-4xl font-bold mb-8 text-center">My Services</h2>

        {/* Grid for large screens */}
        <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-8">
          {services.map((service, index) => (
            <article 
              key={index} 
              className="flex flex-row items-center text-left sm:flex-col sm:text-center p-4"
              aria-labelledby={`service-title-${index}`}
            >
              {/* Icon inside a circle */}
              <div className="flex-shrink-0 flex items-center justify-center w-20 h-20 bg-[#3a4d8a] rounded-full mr-4 sm:mb-4">
                {service.icon}
              </div>
              <div>
                <h3 id={`service-title-${index}`} className="text-lg sm:text-xl font-semibold">{service.title}</h3>
                <p className="text-sm sm:text-base mt-2">{service.description}</p>
              </div>
            </article>
          ))}
        </div>
      </div>
    </section>
  );
}

export default Services;
