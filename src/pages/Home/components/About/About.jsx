import { FaArrowRight } from 'react-icons/fa';

const About = () => {
  return (
    <section 
      id='aboutme' 
      className="text-white py-20 px-8 min-h-screen flex items-center justify-center"
      aria-labelledby="aboutme-title"
    >
      <div className="max-w-4xl mx-auto text-center">
        
        <div className="text-4xl text-[#4d6bfd] mb-6" role="img" aria-label="Data Visualization Icon">📊</div> {/* Optional icon for a hint of personality */}

        <h2 id="aboutme-title" className="text-3xl font-semibold mb-4">
          About Me
        </h2>
        <div className="w-12 h-1 bg-[#4d6bfd] mx-auto mb-8"></div> {/* Minimal divider */}

        <p className="text-lg text-[#b0b3d6] mb-8 leading-relaxed max-w-3xl mx-auto">
          I'm Daniel Gomez, a passionate Data Scientist with over three years of experience transforming data into actionable insights. I specialize in machine learning, data analysis, and building interactive dashboards. Proficient in Python, R, and SQL, I’ve worked on impactful projects in public health, sports analytics, and business intelligence. I strive to make data not just understandable, but also actionable and impactful.
        </p>

        <a
          href="#contact"
          className="text-[#4d6bfd] font-semibold hover:underline hover:text-[#3a5cbb] transition duration-300 inline-flex items-center"
          title="Get in touch with me"
        >
          Get in Touch
          <FaArrowRight className="ml-2" />
        </a>
      </div>
    </section>
  );
};

export default About;
