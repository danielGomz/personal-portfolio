import React from 'react'
import { About, Contact, Hero, Portfolio, Services, Skills } from './components'

const Home = () => {
  return (
    <div>
      <Hero></Hero>
      <About></About>
      <Services></Services>
      <Portfolio></Portfolio>
      <Skills></Skills>
      <Contact></Contact>
    </div>
  )
}

export default Home